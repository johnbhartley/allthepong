import { Mongo } from 'meteor/mongo';

import { Games } from '../api/games.js';
import { Players } from '../api/players.js';
import { Matches } from '../api/matches.js';

Meteor.subscribe('games');
Meteor.subscribe('matches');
Meteor.subscribe('players');

Template.uniquePlayer.helpers({
  iAmMe(user, id) {
    console.log(this);
    if(user && this) {
      console.log(user._id);
      console.log(id);
    }
  },

  allMatches(id) {
    if(id) {
      return Matches.find({$or: [{player1: id}, {player2: id}]}, {sort: {"started": -1}});
    }
  },

  pointsFor(user) {
    //console.log(this);
    if(user) {
      let winningGames = Games.find({$or: [{player1: user}, {player2: user}]});
      let i = 0;
      let total = 0;

      winningGames.forEach(function (row) {
        if(row.player1 != user) {
          i = 1;
        } else {
          i = 0;
        }
        total += row.scores[i];
      });
      return total;
    }
  },

  pointsAgainst(user) {
    //console.log(this);
    if(user) {
      let winningGames = Games.find({$or: [{player1: user}, {player2: user}]});
      let i = 0;
      let total = 0;

      winningGames.forEach(function (row) {
        if(row.player1 != user) {
          i = 0;
        } else {
          i = 1;
        }
        total += row.scores[i];
      });
      return total;
    }
  },

  gamesPlayed(user) {
    return Games.find(
      {$and: [
        {$or: [
          {player1: user},
          {player2: user}
        ]},
      {winner: {$ne: ""}}
      ]}
    ).count();
  },

  matchesPlayed(user) {
    return Matches.find({$or: [{player1: user}, {player2: user}]}).count();
  }


});
