import { Template } from 'meteor/templating';

Template.addGame.helpers({
  players () {
    return Meteor.users.find().fetch();
  }
});

Template.addGame.events({

  'submit .new-match'(event, template) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    // const target = event.target;
    // const text = target.text.value;
    const bestOf = template.find('#bestof').value;
    const gamesTo = template.find('#firstto').value;
    const player1 = template.find('#player1').value;
    const player2 = template.find('#player2').value;

    // whatchoo got
    // console.log('games.insert', bestOf, gamesTo, player1, player2);

    // check to make sure the players are not the same
    if(player1 != player2) {
      Meteor.call('match.insert', bestOf, gamesTo, player1, player2, function(error, result) {

        if(result) {
          Meteor.call('games.insert', player1, player2, '', '', [], result, bestOf, gamesTo, function (err, res) {
            if(err) {

            } else {
              Router.go('/matches/' + result);
            }
          });
        }
      });
    } else {
      alert('The players are the same. Come on guy, stop that.');
    }
  }

});
