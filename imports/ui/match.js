import { Template } from 'meteor/templating';

import { Games } from '../api/games.js';
import { Matches } from '../api/matches.js';

Template.uniqueMatch.helpers({

  games() {
    console.log(Games.find({matchID: this._id}));
    return Games.find({matchID: this._id});
  },

  // player1() {
  //   console.log(Games.find({matchID: this._id}));
  //   const game = Games.find({matchID: this._id});
  // }

  rating(player) {
    if(player) {
      const playerObj = Meteor.users.find({username: player}).fetch();
      return playerObj[0].profile.rating;
    }
  }

});

Template.oneGame.helpers({

  hasScore(scores) {
    if(scores[0] === scores[1]) {
      return false;
    } else {
      return true;
    }
  },

  notNeeded(scores) {
    if(scores[0] === 0 && scores[1] === 0) {
      return true;
    }
  },

  // rating(player) {
  //   if(player) {
  //     const playerObj = Meteor.users.find({username: player}).fetch();
  //     return playerObj[0].profile.rating;
  //   }
  // }

});

Template.uniqueMatch.events({
  'click .submit-scores'(event, template) {

    const rating1 = parseInt(template.find('#player1rating').value, 10);
    const rating2 = parseInt(template.find('#player2rating').value, 10);

    let games = {
      scores: [],
      winner: '',
      loser: '',
      player1wins: 0,
      player2wins: 0,
      matchID: this._id,
      bestOf: this.bestOf,
      player1: this.player1,
      player2: this.player2,
      rating1: rating1,
      rating2: rating2,
      matchWinner: '',
      matchLoser: ''
    }




    // Prevent default browser form submit
    event.preventDefault();

    for(let i = 0; i < games.bestOf; i++) {

      let game = {
        player1score: parseInt(template.find('#game' + i + ' .player1score').value, 10),
        player2score: parseInt(template.find('#game' + i + ' .player2score').value, 10),
        gameID: template.find('#game' + i + ' .gameID').value,
        winner: '',
        loser: '',
      }

      if(game.player1score > game.player2score) {
        game.winner = games.player1;
        game.loser = games.player2;
        games.player1wins++;
        //console.log('player 1 wins' + games.player1wins);
      } else if (game.player2score > game.player1score) {
        game.winner = games.player2;
        game.loser = games.player1;
        games.player2wins++;
        //console.log('player 2 wins' + games.player2wins);
      } else {
        game.player1score = 0;
        game.player2score = 0;
      }

      games.scores[i] = [game.player1score, game.player2score];

      //console.log(game);

      // use this
      Meteor.call('games.update', game.gameID, games.scores[i], game.winner, game.loser);
      Meteor.call('player.addGameWin', game.winner);
      Meteor.call('player.addGameLoss', game.loser);
    }

    if(games.player1wins > games.player2wins) {
      games.matchWinner = games.player1;
      games.loser = games.player2;
    } else {
      games.matchWinner = games.player2;
      games.loser = games.player1;
    }

    let ratingDiff = 0;

    games.rating1 = rating1;
    games.rating2 = rating2;

    if(games.matchWinner == games.player1) {
      ratingDiff = rating1 - rating2;
    } else if (games.matchWinner == games.player2) {
      ratingDiff = rating2 - rating1;
    }


    let ratingInc = 0;
    let ratingMod = 1;

    if(games.bestOf === '1') {
      ratingMod = .5;
    } else if (games.bestOf === '3') {
      ratingMod = .75;
    } else if (games.bestOf === '7') {
      ratingMod = 1.25;
    }

    // high score must come first

    if( ratingDiff < -237 ) {
      ratingInc = 50;
    } else if ( -212 > ratingDiff && ratingDiff > -238 ) {
      ratingInc = 45;
    } else if ( -187 > ratingDiff && ratingDiff > -213 ) {
      ratingInc = 40;
    } else if ( -162 > ratingDiff && ratingDiff > -188 ) {
      ratingInc = 35;
    } else if ( -137 > ratingDiff && ratingDiff > -163 ) {
      ratingInc = 30;
    } else if ( -112 > ratingDiff && ratingDiff > -138 ) {
      ratingInc = 25;
    } else if ( -87 > ratingDiff && ratingDiff > -113 ) {
      ratingInc = 20;
    } else if ( -62 > ratingDiff && ratingDiff > -88 ) {
      ratingInc = 16;
    } else if ( -37 > ratingDiff && ratingDiff > -63 ) {
      ratingInc = 13;
    } else if ( -12 > ratingDiff && ratingDiff > -38 ) {
      ratingInc = 10;
    } else if ( 13 > ratingDiff && ratingDiff > -13 ) {
      ratingInc = 8;
    } else if ( 38 > ratingDiff && ratingDiff > 12 ) {
      ratingInc = 7;
    } else if ( 63 > ratingDiff && ratingDiff > 37 ) {
      ratingInc = 6;
    } else if ( 88 > ratingDiff && ratingDiff > 62 ) {
      ratingInc = 5;
    } else if ( 113 > ratingDiff && ratingDiff > 87 ) {
      ratingInc = 4;
    } else if ( 138 > ratingDiff && ratingDiff > 112) {
      ratingInc = 3;
    } else if ( 188 > ratingDiff && ratingDiff > 137 ) {
      ratingInc = 2;
    } else if ( 238 > ratingDiff && ratingDiff > 187 ) {
      ratingInc = 1;
    } else {
      ratingInc = 0;
    }

    let ratingProd = Math.round(ratingInc*ratingMod);

    Meteor.call('match.update', games.matchID, games.scores, games.matchWinner, games.loser, games.rating1, games.rating2);
    Meteor.call('player.addMatchWin', games.matchWinner, ratingProd);
    Meteor.call('player.addMatchLoss', games.loser, (ratingProd*-1));
    //console.log(games);

  }

});


